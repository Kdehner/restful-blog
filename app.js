const   express     = require('express'),
        app         = express(),
        bp          = require('body-parser'),
        mongoose    = require('mongoose'),
        mo          = require('method-override'),
        expSan      = require('express-sanitizer');

// APP CONFIG
mongoose.connect('mongodb://blog_mongo_1:27017/restful_blog_app', { useNewUrlParser: true })
app.use(bp.urlencoded({extended: true}));
app.use(expSan());
app.use(mo('_method'));
app.use(express.static('public'));
app.set('view engine', 'ejs');

// MONGOOSE/MODEL CONFIG
const blogSchema = new mongoose.Schema({
    title: String,
    image: String,
    body: String,
    created: {type: Date, default: Date.now}
});
const Blog = mongoose.model('Blog', blogSchema);

// RESTFUL ROUTES

app.get('/', (req, res)=>{
    res.redirect('/blogs');
});

// INDEX ROUTE
app.get('/blogs', (req, res)=>{
    Blog.find({})
        .then((blogs)=>{
            res.render('index', {blogs: blogs});
        })
        .catch((err)=>{
            console.log('Error finding blogs in DB', err);
        })
});

// NEW ROUTE
app.get('/blogs/new', (req, res)=>{
    res.render('new');
});

// CREATE ROUTE
app.post('/blogs', (req, res)=>{
    // Sanitize body to allow for safe user HTML use
    req.body.blog.body = req.sanitize(req.body.blog.body);
    // Create new blog post in DB
    Blog.create(req.body.blog)
        .then((blog)=>{
            console.log('New post created', blog);
            // Redirect to index
            res.redirect('/blogs');
        })
        .catch((err)=>{
            console.log('Error creating new post', err);
            res.redirect('/blogs/new');
        })
    
});

// SHOW ROUTE
app.get('/blogs/:id', (req, res)=>{
    Blog.findById(req.params.id)
        .then((blog)=>{
            res.render('show', {blog: blog});
        })
        .catch((err)=>{
            console.log('Error finding blog in DB', err);
            res.redirect('/blogs');
        })
});

// EDIT ROUTE
app.get('/blogs/:id/edit', (req, res)=>{
    Blog.findById(req.params.id)
        .then((blog)=>{
            res.render('edit', {blog: blog});
        })
        .catch((err)=>{
            console.log('Error finding blog in DB', err);
            res.redirect('/blogs');
        })
});

// UPDATE ROUTE
app.put('/blogs/:id', (req, res)=>{
    // Sanitize body to allow for safe user HTML use
    req.body.blog.body = req.sanitize(req.body.blog.body);
    Blog.findByIdAndUpdate(req.params.id, req.body.blog)
        .then((blog)=>{
            res.redirect(`/blogs/${req.params.id}`);
        })
        .catch((err)=>{
            console.log('Error updating blog in DB', err);
            res.redirect('/blogs');
        })
});

// DELETE ROUTE
app.delete('/blogs/:id', (req, res)=>{
    Blog.findByIdAndRemove(req.params.id)
        .then(()=>{
            res.redirect('/blogs');
        })
        .catch((err)=>{
            console.log('Error removing blog in DB', err);
            res.redirect('/blogs');
        })
})

app.listen('3000', ()=>{
    console.log("RESTful blog app has started!");
});
